var express = require('express')
var router = express.Router()

router.get('/', function (req, res) {
    res.send('Pokemon');
})

//loader
var glob = require("glob")
glob("api/*.js", function (er, files) {
    console.log("Loaded " + files.length + " api points")
    files.forEach(function(file){
        router.get('/' + file.replace('.js', ''), require('../' + file))
        console.log(file)
  })
})

module.exports = router